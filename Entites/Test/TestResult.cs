﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entites.Test
{
    public class TestResult
    {
        public string TestDescription { get; set; }
        public bool Succsess { get; set; }
        public string Error { get; set; }
    }
}
