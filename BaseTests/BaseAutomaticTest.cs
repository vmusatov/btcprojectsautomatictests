﻿using Entites.Test;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;

namespace BaseTests
{
    public abstract class BaseAutomaticTest
    {
        private const int FindElementTimeOut = 15; //sec
        public abstract string TestName { get; }

        public abstract TestsResultList ExecuteTests();

        protected PhantomJSDriver GetPhontomDriver(bool mobile = false)
        {
            var service = PhantomJSDriverService.CreateDefaultService(@"D:\PhontomJS\bin");
            //service.HideCommandPromptWindow = true;
            var options = new PhantomJSOptions();

            var driver = new PhantomJSDriver(service, options);

            driver.Manage().Window.Size = mobile ? new Size(320, 568) : new Size(1024, 2000);

            return driver;
        }

        protected ChromeDriver GetChromeDriver(bool mobile = false)
        { 
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            var driver = new ChromeDriver(service);
            driver.Manage().Window.Size = mobile ? new Size(320, 568) : new Size(1024, 2000);

            return driver;
        }

        #region HelperMethods
        protected IWebElement FindElement(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                return element;
            }
            catch
            {
                return null;
            }
        }

        protected bool FindElementAndClick(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                element.Click();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected IWebElement FindElementWithText(IWebDriver driver, By by, string containedText)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        protected IWebElement FindNullableElement(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by);
            }
            catch
            {
                return null;
            }
        }

        protected ReadOnlyCollection<IWebElement> FindNullableElements(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElements(by);
            }
            catch
            {
                return null;
            }
        }
    }
#endregion
}
