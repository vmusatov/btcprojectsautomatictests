﻿using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Entites.Test;
using BaseTests;
using Constants.MC;
using System;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace McTest
{
    [TestClass]
    public class MCAuthTest : BaseAutomaticTest
    {
        public override string TestName => MCTestsName.MCAuthTest;

        public TestResult Auth(string login, string password)
        {
            ChromeDriver driver = GetChromeDriver();
            TestResult result = new TestResult();

            result.TestDescription = $"MC auth with login {login} pass {password}";

            using (driver)
            {
                driver.Navigate().GoToUrl(@"http://mc.lc");

                var userLogin = FindElement(driver, By.Id("UserName"));

                if (userLogin == null)
                {
                    FindElementAndClick(driver, By.CssSelector("a[href = 'javascript:document.getElementById('logoutForm').submit()']"));

                    userLogin = FindElement(driver, By.Id("UserName"));
                }

                var userPass = FindElement(driver, By.Id("Password"));

                if (userLogin == null || userPass == null)
                {
                    result.Succsess = false;
                    result.Error = "userLogin or userPass is null";
                    Console.WriteLine($"Test {result.TestDescription} is {result.Succsess}");
                    return result;
                }

                userLogin.SendKeys(login);
                userPass.SendKeys(password);

                var isLogin = FindElementAndClick(driver, By.CssSelector("input[value *= 'Выполнить вход']"));
                if (!isLogin)
                {
                    result.Succsess = false;
                    result.Error = "Error in login btn click";
                    Console.WriteLine($"Test {result.TestDescription} is {result.Succsess}");
                    return result;
                }

                var balanceTable = FindElement(driver, By.Id("balance-table"), 3);

                if (balanceTable == null)
                {
                    result.Succsess = false;
                    result.Error = "Incorrect login or pass";
                    Console.WriteLine($"Test {result.TestDescription} is {result.Succsess}");
                    return result;
                }

                result.Succsess = true;

                Console.WriteLine($"Test {result.TestDescription} is {result.Succsess}");
                driver.Quit();
            }

            return result;
        }


        public override TestsResultList ExecuteTests()
        {
            var result = new TestsResultList();

            result.Tests.Add(Auth("111", "222"));
            result.Tests.Add(Auth(McConstants.valdLogin, McConstants.validPassword));
            result.Tests.Add(Auth("222fsdf", "dfff"));
            result.Tests.Add(Auth("asd", "123123123"));

            return result;
        }
    }
}
