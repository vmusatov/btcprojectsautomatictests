﻿using BaseTests;
using Constants.MC;
using Entites.Test;
using McTest;
using System.Collections.Generic;

namespace BtcProjectsAutomaticTests
{
    class Program
    {
        private static List<BaseAutomaticTest> AutomaticTests = new List<BaseAutomaticTest>();

        static void Main(string[] args)
        {
            AutomaticTests.Add(new MCAuthTest());

            var result = ExecuteTest(MCTestsName.MCAuthTest, false);
        }

        private static TestsResultList ExecuteTest(string name, bool valid)
        {
            TestsResultList result = new TestsResultList();
            var test = FindTest(name);
            if(test != null)
            {
                result = test.ExecuteTests();     
            }

            return result;
        }

        private static BaseAutomaticTest FindTest(string name)
        { 
            foreach (var test in AutomaticTests)
            {
                if (test.TestName.Equals(name))
                {
                    return test;
                }
            }

            return null;
        }
    }
}
